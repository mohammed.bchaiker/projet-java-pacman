package com.company;

import java.awt.*;

public class Fantome extends Rectangle {

    private int fant_x,fant_y;

    Fantome(int fant_x, int fant_y){
        this.fant_x=fant_x;
        this.fant_y=fant_y;
    }

    public void render(Graphics graphics, Color color){
        graphics.setColor(color);
        graphics.fillRect(fant_x,fant_y,16,16);
    }

}

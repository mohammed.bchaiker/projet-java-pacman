package com.company;

import java.awt.*;
import java.util.Objects;

public class Pacman extends Rectangle {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private int pac_width = 16;
    private int pac_height = 16;
    int pac_x,pac_y;
    public boolean haut,bas,droite,gauche;
    String directiron;




    public Pacman(int pac_x,int pac_y){
        this.pac_x=pac_x;
        this.pac_y=pac_y;
    }

    public void tick(){
        int vitesse = 1;
        Conteneur conteneur = GestionEvenement.conteneur;
        if(Objects.equals(directiron, "Haut") && ProchaineCaseDispo(pac_x,pac_y-vitesse)){
            pac_y=pac_y- vitesse;
        }

        if(Objects.equals(directiron, "Bas") && ProchaineCaseDispo(pac_x,pac_y+vitesse)){
            pac_y=pac_y+ vitesse;
        }

        if(Objects.equals(directiron, "Gauche") && ProchaineCaseDispo(pac_x-vitesse,pac_y)){
            pac_x=pac_x- vitesse;
        }



        if(Objects.equals(directiron, "Droite") && ProchaineCaseDispo(pac_x+vitesse,pac_y)){
            pac_x=pac_x+ vitesse;
        }

        Rectangle p = new Rectangle(pac_x,pac_y,16,16);
        for (int i=0;i<conteneur.gellules.size();i++){

            if(p.intersects(conteneur.gellules.get(i))){
                conteneur.gellules.remove(i);
                break;
            }
        }
        if (conteneur.gellules.size()==0){
            // Gagné
        }
    }

    private boolean ProchaineCaseDispo(int x,int y){

        Rectangle test = new Rectangle(x,y,16,16);
        Case cases[][] = GestionEvenement.conteneur.cases;

        for(int i=0;i<cases.length;i++){
            for (int j=0;j<cases[0].length;j++){
                if (cases[i][j] != null){
                   if (test.intersects(cases[i][j])){
                       return false;
                   }
                }
            }
        }
        return true;
    }


    public void render(Graphics graphics){
        graphics.setColor(Color.yellow);
        graphics.fillRect(pac_x,pac_y,pac_width,pac_height);
    }




}
